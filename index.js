const balance = document.getElementById("balance");
const outstanding = document.getElementById("outstandingAmount");
const outstandingRow = document.getElementById("outstandingRow");
const outstandingAmountFull = document.getElementById("outstandingAmountFull");
const pay = document.getElementById("pay");
const laptopsElement = document.getElementById("laptops");
const price = document.getElementById("price");
const description = document.getElementById("laptopDescription");
const features = document.getElementById("features");
const titleElement = document.getElementById("laptopTitle");
const laptopImage = document.getElementById("laptopImage");
//loanElemetns holds all the elements that should be hidden or visible depending on if you have an poutstanding loan.
const loanElements = document.getElementsByClassName("hide");
//This variable will be used to make sure the user cannot take out more than one loan to buy a computer.
let oneLoanPerComputerReached = false;
//The rootURL is used to get images
const rootURL = "https://noroff-komputer-store-api.herokuapp.com/"

let laptops = [];

//Fetching data from the API and adding each laptop with all its info as an element in laptops.
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptops(laptops));

const addLaptops = (laptops) => {
    laptops.forEach(x => addLaptop(x));
    //Adding all info about the first laptop to the html.
    setAllLaptopElements(laptops[0]);

}
//Adding each laptop's id and title to the select element for the laptops.
const addLaptop = (laptop) => {
    const laptopElement = document.createElement("option");
    laptopElement.value = laptop.id;
    laptopElement.appendChild(document.createTextNode(laptop.title));
    laptopsElement.appendChild(laptopElement);
}

const handleLaptopChoice = e => {
    const selected = laptops[e.target.selectedIndex];
    setAllLaptopElements(selected);
}

const setAllLaptopElements = (selected) => {
    titleElement.textContent = selected.title;
    description.textContent = selected.description;
    price.textContent = selected.price;
    laptopImage.src = rootURL + selected.image;
    features.innerHTML = "";
    //Looping through every element in the "specs" array og this laptop from the API.
    for (const spec of selected.specs) {
        //Adding the info to the correct element and adding <br/> element to create line breaks.
        features.innerHTML += spec + "<br/>";
    }
}

//Listening for change in the select element for the laptops.
laptopsElement.addEventListener("change", handleLaptopChoice);

//Function handles clicking of the "Get a loan" button.
function getALoan() {
    if (oneLoanPerComputerReached) {
        alert("You cannot loan more until you have bought a laptop.");
        return;
    } else if (outstanding.textContent != 0) {
        alert("You cannot loan more until you have paid off your excisting loans.");
        return;
    }
    let amount = window.prompt("Enter amount you wish to loan:");
    if (amount != "" && amount != null) {
        if (amount > balance.textContent*2) {
            alert("You cannot loan more than double your bank balance. Loan not granted.");
            return;
        }
        outstanding.textContent = amount; 
        balance.textContent = +balance.textContent + +amount;
        oneLoanPerComputerReached = true;
        for (element of loanElements) {
            element.style.display = "block"
        }
    }
}

//Function handles clicking of the "Bank" button
function bank() {
    //If there is an outstanding loan, ten percent of pay goes to downpayment.
    if (outstanding.textContent != 0) {
        const tenPercent = +pay.textContent/10;
        const difference = +outstanding.textContent - tenPercent;
        //Checking if the ten percent is more than the outstanding loan.
        //If so the loan will be paid in full, and the surplus of the ten percent go to the bank balance.
        //Also checking if the difference is 0 to also hide the outstanding row and repay button in that case.
        if (difference <= 0) {
            outstanding.textContent = 0;
            pay.textContent = +pay.textContent - tenPercent - difference;
            for (element of loanElements) {
                element.style.display = "none"
            }
        } else {
        outstanding.textContent = +outstanding.textContent - tenPercent;
        pay.textContent = +pay.textContent - tenPercent;
        }
    }
    balance.textContent = +balance.textContent + +pay.textContent;
    pay.textContent = 0;
}
//Onclick for the "Work" button.
function work() {
    pay.textContent = +pay.textContent + 100;
}

function repay() {
    const difference = +outstanding.textContent - +pay.textContent;
    //Checking if pay is more than the outstanding loan.
    //If so, the left over pay stays in pay.
    //Also checking if the difference is 0 to also hide the outstanding row and repay button in that case.
    if (difference <= 0) {
        outstanding.textContent = 0;
        pay.textContent = difference*-1;
        //Hiding the outstanding row since there is no outstanding loan.
        for (element of loanElements) {
            element.style.display = "none"
        }
    } else {
    outstanding.textContent = difference;
    pay.textContent = 0;
    }  
}

//Onclick for the "BUY NOW" button.
function buyNow() {
    if (+balance.textContent >= +price.textContent) {
        balance.textContent = +balance.textContent - +price.textContent;
        alert("You are now the proud new owner of a new laptop!");
        //Making sure the user can loan money for the next computer.
        oneLoanPerComputerReached = false;
    } else {
        alert("You do not have a enough money in your account to buy this computer.");
    }
}